import 'hammerjs';
import {enableProdMode, LOCALE_ID, TRANSLATIONS, TRANSLATIONS_FORMAT} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {AppModule} from './app/app.module';
import {environment} from './environments/environment';
import * as util from 'util';

if (environment.production) {
  enableProdMode();
}

getTranslationProviders().then(providers => {
  const options = {providers};

  platformBrowserDynamic().bootstrapModule(AppModule)
    .catch(err => console.error(err));
});

export function getTranslationProviders(): Promise<Object[]> {
  let locale = 'es';
  const noProviders: Object[] = [];
  if (!locale || locale === 'es') {
    return Promise.resolve(noProviders);
  }
  const translationFile = '../locale/messages.' + locale + '.xlf';
  return getTranslationWithoutImports(translationFile).then((translations: string) =>
    [
      {providers: TRANSLATIONS, useValue: translations},
      {providers: TRANSLATIONS_FORMAT, useValue: 'xlf'},
      {providers: LOCALE_ID, useValue: locale},
    ]
  )
}

export function getTranslationWithoutImports(translationFile: string) {
  return util.getFile(translationFile);
}

