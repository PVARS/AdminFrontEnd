import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AdminLayoutComponent} from './layout/admin-layout/admin-layout.component';
import {LoginComponent} from './components/auth/login/login.component';
import {AuthGuard, LoggedGuard} from './auth/guards';
import {PreviewNewsComponent} from './components/preview-news/preview-news.component';
import {HotpotComponent} from './components/hotpot/hotpot.component';

const routes: Routes = [
  {
    path: '', component: AdminLayoutComponent,
    children: [{
      path: '',
      loadChildren: './layout/admin-layout/routing.module#RoutingModule'
    }],
    canActivate: [AuthGuard]
  },
  {path: 'preview', component: PreviewNewsComponent},
  {path: 'hotpot', component: HotpotComponent},
  {
    path: 'login', component: LoginComponent, canActivate: [LoggedGuard]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
