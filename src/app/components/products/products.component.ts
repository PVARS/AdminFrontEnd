import {Component, OnInit} from '@angular/core';
import {StorageService} from "../../auth/storage.service";
import {ActivatedRoute, Router} from "@angular/router";
import {TranslateService} from "@ngx-translate/core";
import {HttpsServiceService} from "../../service/https-service.service";
import {MatDialog} from "@angular/material/dialog";
import {environment} from '../../../environments/environment.prod';
import {ProductFormComponent} from '../form/product-form/product-form.component';

const PRODUCT_API = environment.apiEndpoint + '/api/product';

@Component({
    selector: 'app-products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  public products: Array<any>;
  public product: any;
  public term: string;

  constructor(private tokenstorage: StorageService,
              private userService: HttpsServiceService, private route: ActivatedRoute,
              public dialog: MatDialog, private router: Router, private token: StorageService,
              public translatee: TranslateService) {
    translatee.setDefaultLang('en');
  }

  openDialog() {
    const dialogRef = this.dialog.open(ProductFormComponent, {
      width: '850px',
      height: '850px',
      data: this.product
    });
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }

  delete(id) {
    this.userService.delete(PRODUCT_API, id).subscribe(result => {
      this.ngOnInit();
    }, error => console.error(error))
  }

  loadStaff(product) {
    this.product = product;
  }

  edit(product) {
    this.product = product;
    this.openDialog();
  }


  ngOnInit() {
    this.product = {};
    this.userService.getAll(PRODUCT_API).subscribe(data => {
      console.log(data);
      this.products = data;
    });
  }

  valuechange(newValue) {
    this.term = newValue;
    if (newValue !== '' || !newValue) {
      this.userService.searchAllColumn(PRODUCT_API, newValue).subscribe(data => {
        this.products = data;
      })
    }
  }
}
