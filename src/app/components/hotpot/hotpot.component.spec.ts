import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HotpotComponent } from './hotpot.component';

describe('HotpotComponent', () => {
  let component: HotpotComponent;
  let fixture: ComponentFixture<HotpotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HotpotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HotpotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
