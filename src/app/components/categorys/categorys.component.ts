import {Component, OnInit} from '@angular/core';
import {StorageService} from '../../auth/storage.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {HttpsServiceService} from '../../service/https-service.service';
import {MatDialog} from '@angular/material';
import {environment} from '../../../environments/environment.prod';
import {CategoryFormComponent} from '../form/category-form/category-form.component';

const CATEGORY_API = environment.apiEndpoint + '/api/category';

@Component({
  selector: 'app-categorys',
  templateUrl: './categorys.component.html',
  styleUrls: ['./categorys.component.css']
})

export class CategorysComponent implements OnInit {
  public categorys: Array<any>;
  public category: any;
  public term: string;

  constructor(private tokenstorage: StorageService,
              private userService: HttpsServiceService, private route: ActivatedRoute,
              public dialog: MatDialog, private router: Router, private token: StorageService,
              public translatee: TranslateService) {
    translatee.setDefaultLang('en');
  }

  openDialog() {
    const dialogRef = this.dialog.open(CategoryFormComponent, {
      width: '850px',
      height: '850px',
      data: this.category
    });
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }

  delete(id) {
    this.userService.delete(CATEGORY_API, id).subscribe(result => {
      this.ngOnInit();
    }, error => console.error(error));
  }

  loadStaff(category) {
    this.category = category;
  }

  edit(category) {
    this.category = category;
    this.openDialog();
  }

  ngOnInit() {
    this.category = {};
    this.userService.getAll(CATEGORY_API).subscribe(data => {
      console.log(data);
      this.categorys = data;
    });
  }

  valuechange(newValue) {
    this.term = newValue;
    if (newValue !== '' || !newValue) {
      this.userService.searchAllColumn(CATEGORY_API, newValue).subscribe(data => {
        this.categorys = data;
      });
    }
  }
}
