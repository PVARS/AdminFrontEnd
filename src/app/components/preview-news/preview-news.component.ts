import {Component, Input, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {environment} from '../../../environments/environment.prod';
import {HttpsServiceService} from '../../service/https-service.service';

const NEWS_API = environment.apiEndpoint + '/api/news';

@Component({
  selector: 'app-preview-news',
  templateUrl: './preview-news.component.html',
  styleUrls: ['./preview-news.component.css']
})
export class PreviewNewsComponent implements OnInit {
  public htmlTemplate: any;
  htmlContent: string;

  constructor(private route: ActivatedRoute,
              private router: Router, private httpService: HttpsServiceService,private view: ViewContainerRef) {
    setTimeout(() => this.htmlContent = (view.element.nativeElement as HTMLElement).innerHTML);
  }

  ngOnInit() {
    this.httpService.get(NEWS_API, this.route.snapshot.queryParamMap.get('id')).subscribe(data => {
      this.htmlTemplate = data.content;
      console.log(data);
      setTimeout(() => this.htmlTemplate = (this.view.element.nativeElement as HTMLElement).innerHTML);
    });
  }

}
