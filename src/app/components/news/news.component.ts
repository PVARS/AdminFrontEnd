import {Component, OnInit} from '@angular/core';
import {environment} from '../../../environments/environment.prod';
import {HttpsServiceService} from '../../service/https-service.service';
import {MatDialog} from '@angular/material';
import {NewsFormComponent} from '../form/news-form/news-form.component';
import {Route, Router} from '@angular/router';

const NEWS_API = environment.apiEndpoint + '/api/news';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
  public newsList: Array<any>;
  public news: any;
  public term: string;

  constructor(private httpService: HttpsServiceService, public dialog: MatDialog, public router: Router) {
  }

  ngOnInit() {
    this.news = {};
    this.httpService.getAll(NEWS_API).subscribe(data => {
      console.log(data);
      this.newsList = data;
    });
  }

  openDialog() {
    const dialogRef = this.dialog.open(NewsFormComponent, {width: '1400px', height: '850px', data: this.news});
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }

  edit(news) {
    this.news = news;
    this.openDialog();
  }

  delete(id) {
    this.httpService.delete(NEWS_API, id).subscribe(result => {
      this.ngOnInit();
    }, error => {
      console.log(error);
    });
  }

  loadNews(news: any) {
    this.news = news;
  }

  preview() {
    // this.route.navigate(['preview', htmlTemplate: this.news.content])
    this.router.navigate(['/preview'], {queryParams: {id: this.news.id}});

  }

  search(value) {
    this.term = value;
    if (value !== '' || !value) {
      this.httpService.searchAllColumn(NEWS_API, value).subscribe(data => {
        this.newsList = data;
      });
    }
  }

}
