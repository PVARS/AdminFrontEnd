import { Component, OnInit } from '@angular/core';
import {environment} from '../../../environments/environment.prod';
import {StorageService} from '../../auth/storage.service';
import {HttpsServiceService} from '../../service/https-service.service';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {TranslateService} from '@ngx-translate/core';
import {PartnerFormComponent} from '../form/partner-form/partner-form.component';

const PARTNER_API = environment.apiEndpoint + '/api/partner';

@Component({
  selector: 'app-partners',
  templateUrl: './partners.component.html',
  styleUrls: ['./partners.component.css']
})
export class PartnersComponent implements OnInit {

  public partners: Array<any>;
  public partner: any;
  public term: string;

  constructor(private tokenstorage: StorageService,
              private userService: HttpsServiceService, private route: ActivatedRoute,
              public dialog: MatDialog, private router: Router, private token: StorageService,
              public translatee: TranslateService) {
    translatee.setDefaultLang('en');
  }

  openDialog() {
    const dialogRef = this.dialog.open(PartnerFormComponent, {
      width: '850px',
      height: '850px',
      data: this.partner
    });
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }

  delete(id) {
    this.userService.delete(PARTNER_API, id).subscribe(result => {
      this.ngOnInit();
    }, error => console.error(error))
  }

  loadStaff(partner) {
    this.partner = partner;
  }

  edit(partner) {
    this.partner = partner;
    this.openDialog();
  }

  ngOnInit() {
    this.partner = {};
    this.userService.getAll(PARTNER_API).subscribe(data => {
      console.log(data);
      this.partners = data;
    });
  }

  valuechange(newValue) {
    this.term = newValue;
    if (newValue !== '' || !newValue) {
      this.userService.searchAllColumn(PARTNER_API, newValue).subscribe(data => {
        this.partners = data;
      });
    }
  }
}
