import {Component, OnInit} from '@angular/core';
import {StorageService} from "../../auth/storage.service";
import {HttpsServiceService} from "../../service/https-service.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {TranslateService} from "@ngx-translate/core";
import {environment} from "../../../environments/environment.prod";
import {ProjectFormComponent} from "../form/project-form/project-form.component";

const PROJECT_API = environment.apiEndpoint + '/api/project';

@Component({
    selector: 'app-projects',
    templateUrl: './projects.component.html',
    styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {
    public projects: Array<any>;
    public project: any;
    public term: string;
    constructor(private tokenstorage: StorageService,
                private userService: HttpsServiceService, private route: ActivatedRoute,
                public dialog: MatDialog, private router: Router, private token: StorageService,) {
    }

    openDialog() {
        const dialogRef = this.dialog.open(ProjectFormComponent, {
            width: '850px',
            height: '850px',
            data: this.project
        });
        dialogRef.afterClosed().subscribe(result => {
            this.ngOnInit();
        });
    }

    delete(id) {
        this.userService.delete(PROJECT_API, id).subscribe(result => {
            this.ngOnInit();
        }, error => console.error(error))
    }

    loadProject(project) {
        this.project = project;
    }

    edit(project) {
        this.project = project;
        this.openDialog();
    }

    ngOnInit() {
        this.project = {};
        this.userService.getAll(PROJECT_API).subscribe(data => {
            console.log(data);
            this.projects = data;
        });
    }

    valueChange(newValue) {
        this.term = newValue;
        if (newValue !== '' || !newValue) {
            this.userService.searchAllColumn(PROJECT_API, newValue).subscribe(data => {
                this.projects = data;
            })
        }
    }

}
