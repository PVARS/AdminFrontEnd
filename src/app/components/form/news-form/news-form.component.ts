import {Component, Inject, OnInit} from '@angular/core';
import * as DecoupledEditor from '@ckeditor/ckeditor5-build-decoupled-document';
import {FormControl} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {HttpsServiceService} from '../../../service/https-service.service';
import {environment} from '../../../../environments/environment.prod';
import {UploadAdapter} from '../../../service/upload-adapter';

const NEWS_API = environment.apiEndpoint + '/api/news';

@Component({
  selector: 'app-news-form',
  templateUrl: './news-form.component.html',
  styleUrls: ['./news-form.component.css']
})
export class NewsFormComponent implements OnInit {
  public Editor = DecoupledEditor;
  selectedFiles: FileList;
  public model = {
    editorData: '<p>Hello, world!</p>'
  };
  text: string;
  constructor(public dialogRef: MatDialogRef<any>,
              private userService: HttpsServiceService, @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
  }

  public onReady(editor) {
    if (!(this.data.content == '' || !this.data.content)) {
      this.model.editorData = this.data.content;
    }
    console.log(this.model);
    editor.ui.getEditableElement().parentElement.insertBefore(
      editor.ui.view.toolbar.element,
      editor.ui.getEditableElement()
    );
    editor.plugins.get('FileRepository').createUploadAdapter = (loader) => {
      return new UploadAdapter(loader);
    };

  }

  readThis(inputValue: any, index: number): void {
    var file: File = inputValue.files[0];
    var myReader: FileReader = new FileReader();
    myReader.onloadend = (e) => {
      this.data.image = myReader.result;
    };
    myReader.readAsDataURL(file);
  }

  selectFile(event, index: number) {
    const file = event.target.files.item(0);
    if (event.target.files.length > 0 && file.type.match('image.*')) {
      this.selectedFiles = event.target.files;
    } else {
      alert('Invaild Format');
    }
    if (event.target.files.length > 0) {
      this.readThis(event.target, index);
    }
  }

  onNoClick() {
    this.dialogRef.close();
  }

  save() {
    console.log(this.data);
    this.userService.save(NEWS_API, this.data).subscribe(resResult => {
      this.dialogRef.close();
    }, error => console.error(error));
  }
}
