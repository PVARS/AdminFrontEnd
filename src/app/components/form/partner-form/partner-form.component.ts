import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {HttpsServiceService} from '../../../service/https-service.service';
import {environment} from '../../../../environments/environment.prod';

const PARTNER_API = environment.apiEndpoint + '/api/partner';

@Component({
  selector: 'app-partner-form',
  templateUrl: './partner-form.component.html',
  styleUrls: ['./partner-form.component.css']
})
export class PartnerFormComponent implements OnInit {
  date = new FormControl(new Date());
  selectedFiles: FileList;
  selected = 'option2';
  typeOptions: Array<any>;
  validated = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    doiTac: new FormControl('', [Validators.required]),
    linhVuc: new FormControl('', [Validators.required]),
    diaChi: new FormControl('', [Validators.required]),
    sDT: new FormControl('', [Validators.required, Validators.pattern('^((\\\\+91-? )|0)?[0-9 ]{10}$')])
  });

  get email() {
    return this.validated.get('email');
  }
  get doiTac() {
    return this.validated.get('doiTac');
  }
  get linhVuc() {
    return this.validated.get('linhVuc');
  }
  get diaChi() {
    return this.validated.get('diaChi');
  }
  get sDT() {
    return this.validated.get('sDT');
  }

  submitted = false;

  constructor(public dialogRef: MatDialogRef<any>,
              private userService: HttpsServiceService, @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }


  readThis(inputValue: any, index: number): void {
    var file: File = inputValue.files[0];
    var myReader: FileReader = new FileReader();
    myReader.onloadend = (e) => {
      this.data.logo = myReader.result;
    };
    myReader.readAsDataURL(file);
  }

  selectFile(event, index: number) {
    const file = event.target.files.item(0);
    if (event.target.files.length > 0 && file.type.match('image.*')) {
      this.selectedFiles = event.target.files;
    } else {
      alert('Invaild Format');
    }
    if (event.target.files.length > 0) {
      this.readThis(event.target, index);
    }
  }

  public bankCtrl: FormControl = new FormControl();

  public bankFilterCtrl: FormControl = new FormControl();

  ngOnInit() {

    this.typeOptions = ['Khu nghỉ dưỡng', 'Đất nền', 'Nhà phố', 'Căn hộ'];
    this.bankCtrl.setValue(this.data.loaiHinh);
    if (this.data.ngayBatDau) {
      this.date.setValue(this.data.ngayBatDau);
    }
  }

  onNoClick() {
    this.dialogRef.close();
  }

  onSubmit() {
    console.warn(this.validated);
  }

  save() {
    this.data.ngayBatDau = this.date.value;
    console.log(this.data);
    this.userService.save(PARTNER_API, this.data).subscribe(resResult => {
      this.dialogRef.close();
    }, error => console.error(error));
  }

}
