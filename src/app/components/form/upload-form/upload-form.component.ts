import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {HttpsServiceService} from "../../../service/https-service.service";
import {FormControl} from "@angular/forms";
import {environment} from "../../../../environments/environment.prod";

const PRODUCT_API = environment.apiEndpoint + '/api/product';
const UPLOAD_API = environment.apiEndpoint + '/api/image';
@Component({
  selector: 'app-upload-form',
  templateUrl: './upload-form.component.html',
  styleUrls: ['./upload-form.component.css']
})
export class UploadFormComponent implements OnInit {
  typeOptions: Array<any>;
  selectedFiles: FileList;
  productID: Array<any>;

  constructor(public dialogRef: MatDialogRef<any>,
              private userService: HttpsServiceService, @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  public bankCtrl: FormControl = new FormControl();

  ngOnInit() {
    this.typeOptions = ["Ảnh 360", "Ảnh thường"];
    this.userService.getAll(PRODUCT_API).subscribe(data => {
        // console.log(data);
        this.productID = data;
      }
    )
  }
  onNoClick() {
    this.dialogRef.close();
  }
  readThis(inputValue: any, index: number): void {
    var file: File = inputValue.files[0];
    var myReader: FileReader = new FileReader();
    myReader.onloadend = (e) => {
      this.data.image = myReader.result;
    };
    myReader.readAsDataURL(file);
  }
  save() {
    console.log(this.data);
    this.userService.save(UPLOAD_API, this.data).subscribe(resResult => {
      this.dialogRef.close();
    }, error => console.error(error));
  }
  selectFile(event, index: number) {
    const file = event.target.files.item(0);
    if (event.target.files.length > 0 && file.type.match('image.*')) {
      this.selectedFiles = event.target.files;
    } else {
      alert('Invaild Format');
    }
    if (event.target.files.length > 0) {
      this.readThis(event.target, index);
    }
  }
}
