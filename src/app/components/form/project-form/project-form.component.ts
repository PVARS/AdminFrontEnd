import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {HttpsServiceService} from "../../../service/https-service.service";
import {FormControl} from "@angular/forms";
import {environment} from "../../../../environments/environment.prod";

const PROJECT_API = environment.apiEndpoint + '/api/project';

@Component({
    selector: 'app-project-form',
    templateUrl: './project-form.component.html',
    styleUrls: ['./project-form.component.css']
})
export class ProjectFormComponent implements OnInit {
    date = new FormControl(new Date());
    selectedFiles: FileList;
    selected = 'option2';
    typeOptions: Array<any>;
    constructor(public dialogRef: MatDialogRef<any>,
                private userService: HttpsServiceService, @Inject(MAT_DIALOG_DATA) public data: any) {
    }


    public bankCtrl: FormControl = new FormControl();

    public bankFilterCtrl: FormControl = new FormControl();

    ngOnInit() {
        this.typeOptions = ["Khu nghỉ dưỡng", "Đất nền", "Nhà phố", "Căn hộ"];
        this.bankCtrl.setValue(this.data.loaiHinh);
        if (this.data.ngayBatDau) {
            this.date.setValue(this.data.ngayBatDau);
        }
    }

    onNoClick() {
        this.dialogRef.close();
    }

    save() {
        this.data.ngayBatDau = this.date.value;
        this.userService.save(PROJECT_API, this.data).subscribe(resResult => {
            this.dialogRef.close();
        }, error => console.error(error));
    }

  readThis(inputValue: any, index: number): void {
    var file: File = inputValue.files[0];
    var myReader: FileReader = new FileReader();
    myReader.onloadend = (e) => {
      this.data.image = myReader.result;
    };
    myReader.readAsDataURL(file);
  }

  selectFile(event, index: number) {
    const file = event.target.files.item(0);
    if (event.target.files.length > 0 && file.type.match('image.*')) {
      this.selectedFiles = event.target.files;
    } else {
      alert('Invaild Format');
    }
    if (event.target.files.length > 0) {
      this.readThis(event.target, index);
    }
  }
}
