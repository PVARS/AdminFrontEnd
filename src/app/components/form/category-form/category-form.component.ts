import {Component, Inject, OnInit} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {FormControl} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {HttpsServiceService} from '../../../service/https-service.service';

const CATEGORY_API = environment.apiEndpoint + '/api/category';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.css']
})
export class CategoryFormComponent implements OnInit {
  date = new FormControl(new Date());
  selectedFiles: FileList;
  selected = 'option2';
  typeOptions: Array<any>;
  constructor(public dialogRef: MatDialogRef<any>,
              private userService: HttpsServiceService, @Inject(MAT_DIALOG_DATA) public data: any) {
  }


  readThis(inputValue: any, index: number): void {
    var file: File = inputValue.files[0];
    var myReader: FileReader = new FileReader();
    myReader.onloadend = (e) => {
      this.data.image = myReader.result;
    };
    myReader.readAsDataURL(file);
  }

  selectFile(event, index: number) {
    const file = event.target.files.item(0);
    if (event.target.files.length > 0 && file.type.match('image.*')) {
      this.selectedFiles = event.target.files;
    } else {
      alert('Invaild Format');
    }
    if (event.target.files.length > 0) {
      this.readThis(event.target, index);
    }
  }

  public bankCtrl: FormControl = new FormControl();

  public bankFilterCtrl: FormControl = new FormControl();

  ngOnInit() {
    this.typeOptions = ["Khu nghỉ dưỡng", "Đất nền", "Nhà phố", "Căn hộ"];
    this.bankCtrl.setValue(this.data.loaiHinh);
    if (this.data.ngayBatDau) {
      this.date.setValue(this.data.ngayBatDau);
    }
  }

  onNoClick() {
    this.dialogRef.close();
  }

  save() {
    this.data.ngayBatDau = this.date.value;
    console.log(this.data);
    this.userService.save(CATEGORY_API, this.data).subscribe(resResult => {
      this.dialogRef.close();
    }, error => console.error(error));
  }

}
