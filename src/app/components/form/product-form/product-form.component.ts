import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {HttpsServiceService} from "../../../service/https-service.service";
import {FormControl} from "@angular/forms";
import {environment} from "../../../../environments/environment.prod";

const PRODUCT_API = environment.apiEndpoint + '/api/product';
const PROJECT_API = environment.apiEndpoint + '/api/project';
@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {
  date = new FormControl(new Date());
  nameProject: Array<any>;
  selectedFiles: FileList;
  selected = 'option2';
  typeOptions: Array<any>;
  constructor(public dialogRef: MatDialogRef<any>,
              private userService: HttpsServiceService, @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  readThis(inputValue: any, index: number): void {
    var file: File = inputValue.files[0];
    var myReader: FileReader = new FileReader();
    myReader.onloadend = (e) => {
      this.data.image = myReader.result;
    };
    myReader.readAsDataURL(file);
  }

  selectFile(event, index: number) {
    const file = event.target.files.item(0);
    if (event.target.files.length > 0 && file.type.match('image.*')) {
      this.selectedFiles = event.target.files;
    } else {
      alert('Invaild Format');
    }
    if (event.target.files.length > 0) {
      this.readThis(event.target, index);
    }
  }

  public bankCtrl: FormControl = new FormControl();

  public bankFilterCtrl: FormControl = new FormControl();

  ngOnInit() {
    // this.typeOptions = ["Khu nghỉ dưỡng", "Đất nền", "Nhà phố", "Căn hộ"];
    this.userService.getAll(PROJECT_API).subscribe(data => {
        // console.log(data);
        this.nameProject = data;
      }
    )
    this.bankCtrl.setValue(this.data.loaiHinh);
    if (this.data.ngayTao) {
      this.date.setValue(this.data.ngayTao);
    }
  }

  onNoClick() {
    this.dialogRef.close();
  }

  save() {
    this.data.ngayTao = this.date.value;
    console.log(this.data);
    this.userService.save(PRODUCT_API, this.data).subscribe(resResult => {
      this.dialogRef.close();
    }, error => console.error(error));
  }
}
