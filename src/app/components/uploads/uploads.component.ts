
import {Component, OnInit} from '@angular/core';
import {StorageService} from "../../auth/storage.service";
import {HttpsServiceService} from "../../service/https-service.service";
import {ActivatedRoute, Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {TranslateService} from "@ngx-translate/core";
import {environment} from "../../../environments/environment.prod";
import {ProjectFormComponent} from "../form/project-form/project-form.component";
import {UploadFormComponent} from "../form/upload-form/upload-form.component";

const UPLOAD_API = environment.apiEndpoint + '/api/image';

@Component({
  selector: 'app-uploads',
  templateUrl: './uploads.component.html',
  styleUrls: ['./uploads.component.css']
})
export class UploadsComponent implements OnInit {

  public uploads: Array<any>;
  public upload: any;
  public term: string;

  constructor(private tokenstorage: StorageService,
              private userService: HttpsServiceService, private route: ActivatedRoute,
              public dialog: MatDialog, private router: Router, private token: StorageService,
              public translatee: TranslateService) {
    translatee.setDefaultLang('en');
  }

  openDialog() {
    const dialogRef = this.dialog.open(UploadFormComponent, {
      width: '850px',
      height: '850px',
      data: this.upload
    });
    dialogRef.afterClosed().subscribe(result => {
      this.ngOnInit();
    });
  }

  delete(id) {
    this.userService.delete(UPLOAD_API, id).subscribe(result => {
      this.ngOnInit();
    }, error => console.error(error))
  }

  loadStaff(upload) {
    this.upload = upload;
  }

  edit(upload) {
    this.upload = upload;
    this.openDialog();
  }
  ngOnInit() {
    this.upload = {};
    this.userService.getAll(UPLOAD_API).subscribe(data => {
      console.log(data);
      this.uploads = data;
    });
  }
  valuechange(newValue) {
    this.term = newValue;
    if (newValue !== '' || !newValue) {
      this.userService.searchAllColumn(UPLOAD_API, newValue).subscribe(data => {
        this.uploads = data;
      })
    }
  }

}
