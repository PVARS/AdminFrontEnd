import {Component, OnInit} from '@angular/core';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {ROUTES} from "../sidebar/sidebar.component";
import {ActivatedRoute, Router} from "@angular/router";
import {StorageService} from "../../auth/storage.service";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  private listTitles: any[];
  location: Location;

  constructor(location: Location, private router: Router, private route: ActivatedRoute, private token: StorageService,
              public translate: TranslateService) {
    translate.setDefaultLang('vn');
    this.location = location;
  }

  switchLanguage(language: string) {
    this.translate.use(language);
  }

  ngOnInit() {
    this.listTitles = ROUTES.filter(listTitle => listTitle);
  }

  getTitle() {
    var titleHeader = this.location.prepareExternalUrl(this.location.path());
    if (titleHeader.charAt(0) === '#') {
      titleHeader = titleHeader.slice(1);
    }
    for (var item = 0; item < this.listTitles.length; item++) {
      if (this.listTitles[item].path === titleHeader) {
        return this.listTitles[item].title;
      }
    }
  }

  goBack(): void {
    this.token.signOut();
    this.router.navigate(['login'], {relativeTo: this.route.parent});
  }
}
