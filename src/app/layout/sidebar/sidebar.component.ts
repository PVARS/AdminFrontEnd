import {Component, OnInit} from '@angular/core';

declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}

export const ROUTES: RouteInfo[] = [
  {path: '/projects', title: "Projects", icon: 'add_business', class: ''},
  {path: '/products', title: "Product", icon: 'meeting_room', class: ''},
  {path: '/news', title: 'News', icon: 'receipt_long', class: ''},
  {path: '/partner', title: 'partner', icon: 'star_border', class: ''},
  {path: '/categories', title: 'Categories', icon: 'category', class: ''},
  {path: '/uploads-image', title: 'Upload-image ', icon: 'panorama', class: ''},
  {path: '/hotpot', title: 'hotpot ', icon: '360', class: ''}
];
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() {
  }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }

}
