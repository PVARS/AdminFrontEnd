import {Routes} from "@angular/router";
import {ProductsComponent} from "../../components/products/products.component";
import {ProjectsComponent} from "../../components/projects/projects.component";
import {NewsComponent} from '../../components/news/news.component';
import {CategorysComponent} from '../../components/categorys/categorys.component';
import {PartnersComponent} from '../../components/partners/partners.component';
import {UploadsComponent} from "../../components/uploads/uploads.component";
import {HotpotComponent} from '../../components/hotpot/hotpot.component';

export const AdminLayoutRoutes: Routes = [
  {path: 'products', component: ProductsComponent},
  {path: 'projects', component: ProjectsComponent},
  {path: 'news', component: NewsComponent},
  {path: 'categories', component: CategorysComponent},
  {path: 'partner', component: PartnersComponent},
  {path: 'uploads-image', component: UploadsComponent},
];
