import { Component, OnInit } from '@angular/core';
import {StorageService} from "../../auth/storage.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.css']
})
export class AdminLayoutComponent implements OnInit {

  constructor(private tokenstorage: StorageService, private router: Router) {
  }

  ngOnInit() {

  }

}
