import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {AdminLayoutRoutes} from './admin-layout.routing';
import {FormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {ProductsComponent} from '../../components/products/products.component';
import {ProjectsComponent} from '../../components/projects/projects.component';
import {DemoMaterialModule} from '../../material-module';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatButtonModule} from '@angular/material/button';
import {NewsComponent} from '../../components/news/news.component';
import {CategorysComponent} from '../../components/categorys/categorys.component';
import {PartnersComponent} from '../../components/partners/partners.component';
import {UploadsComponent} from '../../components/uploads/uploads.component';
import {HotpotComponent} from '../../components/hotpot/hotpot.component';
import {MatSliderModule} from '@angular/material';


@NgModule({
  declarations: [
    ProductsComponent,
    ProjectsComponent,
    NewsComponent,
    CategorysComponent, PartnersComponent, UploadsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatSliderModule,
    TranslateModule
  ],
})
export class RoutingModule {
}
