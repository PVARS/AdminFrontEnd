import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AdminLayoutComponent} from './layout/admin-layout/admin-layout.component';
import {LoginComponent} from './components/auth/login/login.component';
import {DemoMaterialModule} from './material-module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {ProductsComponent} from './components/products/products.component';
import {SidebarComponent} from './layout/sidebar/sidebar.component';
import {NavbarComponent} from './layout/navbar/navbar.component';
import {AuthGuard, LoggedGuard} from './auth/guards';
import {ProjectsComponent} from './components/projects/projects.component';
import {httpInterceptorProviders} from './auth/auth-interceptor';
import {ProjectFormComponent} from './components/form/project-form/project-form.component';
import {RoutingModule} from './layout/admin-layout/routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {ProductFormComponent} from './components/form/product-form/product-form.component';
import {NewsFormComponent} from './components/form/news-form/news-form.component';
import {PreviewNewsComponent} from './components/preview-news/preview-news.component';
import {CategoryFormComponent} from './components/form/category-form/category-form.component';
import {PartnerFormComponent} from './components/form/partner-form/partner-form.component';
import {UploadFormComponent} from './components/form/upload-form/upload-form.component';
import {HotpotComponent} from './components/hotpot/hotpot.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    LoginComponent,
    SidebarComponent,
    NavbarComponent,
    ProjectFormComponent,
    ProductFormComponent,
    NewsFormComponent,
    PreviewNewsComponent,
    CategoryFormComponent,
    PartnerFormComponent,
    UploadFormComponent,
    HotpotComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    DemoMaterialModule,
    ReactiveFormsModule,
    BrowserModule,
    ReactiveFormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
  ], entryComponents: [
    ProjectFormComponent, ProductFormComponent, NewsFormComponent, CategoryFormComponent, PartnerFormComponent, UploadFormComponent
  ],
  providers: [
    AuthGuard,
    LoggedGuard, httpInterceptorProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
